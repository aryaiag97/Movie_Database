package com.company;

import com.company.classes.Movie;
import com.company.classes.TvShow;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        boolean isRunning = true;

        while (isRunning) {
            System.out.println("Selamat datang di Movie Database");
            System.out.println("1. Masukkan Film");
            System.out.println("2. Masukkan Acara Tv");
            System.out.println("3. Informasi Film");
            System.out.println("4. Informasi Acara Tv");
            System.out.println("5. Exit");
            System.out.println("Masukkan pilihan anda : ");

            String choice = input.nextLine();

            switch (choice) {
                case "1" -> {
                    System.out.println("Masukkan judul : ");
                    String judul = input.nextLine();
                    System.out.println("Masukkan sinopsis : ");
                    String sinopsis = input.nextLine();
                    System.out.println("Masukkan genre : ");
                    String genre = input.nextLine();
                    System.out.println("Masukkan rating : ");
                    String rating = input.nextLine();
                    System.out.println("Masukkan tanggal rilis : ");
                    String tanggalRilis = input.nextLine();
                    System.out.println("Masukkan durasi (dalam menit): ");
                    int durasi = input.nextInt();
                    Movie movie = new Movie(judul, sinopsis, genre, rating, tanggalRilis, durasi);
                    movie.writeFile();
                }
                case "2" -> {
                    System.out.println("Masukkan judul : ");
                    String judulTv = input.nextLine();
                    System.out.println("Masukkan sinopsis : ");
                    String sinopsisTv = input.nextLine();
                    System.out.println("Masukkan genre : ");
                    String genreTv = input.nextLine();
                    System.out.println("Masukkan rating : ");
                    String ratingTv = input.nextLine();
                    System.out.println("Masukkan jumlah episode: ");
                    int eps = input.nextInt();
                    TvShow tv = new TvShow(judulTv, sinopsisTv, genreTv, ratingTv, eps);
                    tv.writeFile();
                }
                case "3" -> {
                    Movie.readFile();
                }
                case "4" -> {
                    TvShow.readFile();
                }
                case "5" -> {
                    isRunning = false;
                }
                default -> System.out.println("Invalid");
            }
            System.out.println("Success...");
        }
    }
}
