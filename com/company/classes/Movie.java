package com.company.classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class Movie extends Show{
    private String tanggalRilis;
    private int durasi;
    public Movie(String judul, String sinopsis, String genre, String rating, String tanggalRilis, int durasi) {
        super(judul, sinopsis, genre, rating);
        this.tanggalRilis = tanggalRilis;
        this.durasi = durasi;
    }

    public String getTanggalRilis(){
        return this.tanggalRilis;
    }
    public int getDurasi(){
        return this.durasi;
    }
    public void setTanggalRilis(String tanggalRilis){
        this.tanggalRilis = tanggalRilis;
    }
    public void setDurasi(int durasi){
        this.durasi = durasi;
    }

    public void writeFile(){
        String text = """
            Judul: %s,
            Sinopsis: %s,
            Genre: %s,
            Tanggal Rilis: %s,
            Rating: %s,
            Durasi: %d menit
            
            """;
        String formattedText = String.format(text, this.getJudul(), this.getSinopsis(), this.getGenre(), this.tanggalRilis, this.getRating(), this.durasi );
        try{
            FileWriter fw= new FileWriter("D:/projekAkhirPBO/src/com/company/text/movie.txt", true);
            fw.write(formattedText);
            fw.close();
        }catch(Exception e){
             System.out.println(e.getMessage());
         }
        System.out.println("Success...");
    }
    public static void readFile(){
        try {
            File file = new File("D:/projekAkhirPBO/src/com/company/text/movie.txt");
            Scanner read = new Scanner(file);
            while (read.hasNextLine()) {
                String value = read.nextLine();
                System.out.println(value);
            }
            read.close();
        }catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}


