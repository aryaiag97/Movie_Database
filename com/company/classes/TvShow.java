package com.company.classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class TvShow extends Show {
    private int eps;
    public TvShow(String judul, String sinopsis, String genre, String rating, int eps) {
        super(judul, sinopsis, genre, rating);
        this.eps = eps;
    }
    public int getEps(){
        return this.eps;
    }
    public void setEps(int eps){
        this.eps = eps;
    }

    public void writeFile(){
        String text = """
        Judul: %s,
        Sinopsis: %s,
        Genre: %s,
        Rating: %s,
        Episode: %d
    
        """;

        String formattedText = String.format(text, this.getJudul(), this.getSinopsis(), this.getGenre(), this.getRating(), this.eps );
        try{
            FileWriter fw= new FileWriter("D:/projekAkhirPBO/src/com/company/text/tvshow.txt", true);
            fw.write(formattedText);
            fw.close();
        }catch(Exception e){
             System.out.println(e.getMessage());
         }
        System.out.println("Success...");
    }
    public static void readFile(){
        try {
            File file = new File("D:/projekAkhirPBO/src/com/company/text/tvshow.txt");
            Scanner read = new Scanner(file);
            while (read.hasNextLine()) {
                String value = read.nextLine();
                System.out.println(value);
            }
            read.close();
        }catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
